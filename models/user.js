const Mongoose = require('mongoose'),
	ObjectId = Mongoose.Schema.Types.ObjectId

const UserSchema = new Mongoose.Schema({
	uID: String,
	password: String,
	email: String,
	firstName: String,
	lastName: String,
	nickName: String,
	company: String,
	website: String,
	sendNewsletter: Boolean,
	session: {type: ObjectId, ref: 'Session'},
	registeredOn: {type: Date, Default: Date.now},
	registeredFrom: String,
	purchases: [{type: ObjectId, ref: 'Purchase'}],
	cart: {type: ObjectId, ref: 'Cart'}
})

const SessionSchema = new Mongoose.Schema({
	start: {type: Date, default: Date.now},
	end: {type: Date, default: Date.now},
	ip: String,
	user: {type: ObjectId, ref: 'User'},
	cart: [{type: ObjectId, ref: 'Product'}],
	key: String
})


Mongoose.model('User', UserSchema)
Mongoose.model('Session', SessionSchema)