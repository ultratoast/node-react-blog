const Mongoose = require('mongoose'),
	ObjectId = Mongoose.Schema.Types.ObjectId

const PostSchema = new Mongoose.Schema({
	title: String,
	author: {type: ObjectId,ref:'User'},
	content: String,
	postDate: {type: Date, default: Date.now},
	isPublic: Boolean,
	type: String,
	categories: {
		name: String
	},
	upVotes: Number,
	replies: [{type: ObjectId, ref: 'Reply'}]
})

const ReplySchema = new Mongoose.Schema({
	author: String,
	content: String,
	postDate: {type: Date, default: Date.now},
	upVotes: Number,
	thread: {type: ObjectId,ref: 'Post'}
})


Mongoose.model('Post',PostSchema)
Mongoose.model('Reply',ReplySchema)