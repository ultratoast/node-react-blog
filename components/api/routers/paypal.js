const Express = require('express'),
	Router = Express.Router(),
	db = require('../helpers/db'),
	Mongoose = require('mongoose'),
	renderJSON = require('../helpers/renderJSON')

module.exports = Router