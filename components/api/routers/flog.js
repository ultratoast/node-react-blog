const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	renderJSON = require('../helpers/renderJSON')

Router.route('/:type/all')
	.get((req,res) => {
		let findStatement = {
			type: req.params.type
		}
		dbHelper.leanFind('Post',findStatement,(err,posts) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',posts)
			}
		})
	})

Router.route('/:type/latest')
	.get((req,res) => {
		let now = new Date(),
			pastMonth = now - 2628002880,
			findStatement = {
				type: req.params.type,
				date: {gte:now,lte:pastMonth}
			}
		dbHelper.leanFind('Post',findStatement,(err,posts) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,err)
			}
		})
	})


Router.route('/:type/add')
	.post((req,res) => {
		let postDate = new Date(),
			categories = []
		req.body.categories.map((category,i) => {
			categories.push(category)
		})
		let createStatement = {
			title: req.body.title,
			author: req.body.author,
			postDate: postDate,
			content: req.body.content,
			isPublic: req.body.isPublic,
			catgeories: catgeories,
			upvotes: 0
		}
		dbHelper.addOne('Post',createStatement, (err,post) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',post)
			}
		})
	})

Router.route('/:type/:id')
	.get((req,res) => {
		dbHelper.findOne('Post',req.params.id, (err,post) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',post)
			}
		})
	})

Router.route('/:type/:id/upvote')
	.post((req,res) => {
		let upvotes = req.body.upvotes,
			updateStatement = {
				upvotes: upvotes
			}
		dbHelper.updateOne('Post',req.params.id,updateStatement, (err,post) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',post)
			}
		})
	})

Router.route('/:type/:id/reply')
	.post((req,res) => {
		let now = new Date(),
			createStatement = {
				author: req.body.author,
				content: req.body.content,
				postDate: now,
				upvotes: 0,
				thread: req.params.id
			}
		dbHelper.addOne('Reply',createStatement,(err,reply) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',reply)
			}
		})
	})

Router.route('/:type/:id/reply/:replyid')
	//handle reply upvotes
	.post((req,res) => {
		let upvotes = req.body.upvotes,
			updateStatement = {
				upvotes: upvotes
			} 
		dbHelper.updateOne('Reply',req.params.replyid, updateStatement, (err,reply) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',reply)
			}
		})
	})
	//handle reply deletion
	.delete((req,res) => {
		dbHelper.deleteOne('Reply',req.params.id, (err,reply) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',reply)
			}
		})
	})


Router.route('/:type/:id/edit')
	.post((req,res) => {
		let categories = []
		req.body.categories.map((category,i) => {
			categories.push(category)
		})
		let updateStatement = {
			title: req.bodytitle,
			content: req.bodycontent,
			isPublic: req.bodyisPublic,
			categories: catgeories
		}
		dbHelper.updateOne('Post',req.params.id,updateStatement, (err,post) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',post)
			}
		})
	})

Router.route('/:type/:id/delete')
	.delete((req,res) => {
		dbHelper.deleteOne('Post',req.params.id, (err,post) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',post)
			}
		})
	})

module.exports = Router