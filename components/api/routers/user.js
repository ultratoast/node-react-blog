const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	renderJSON = require('../helpers/renderJSON'),
	guid = require('../helpers/guid'),
	crypty = require('../helpers/crypty')

Router.route('/login') //verify account + password + start session
	.post((req,res) => {
		//first find the user
		dbHelper.findOneByKey('User',{email:req.bodyemail}, (err,user) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				let user = user,
				password = crypty.cipher(req.body.password)
				if (password === user.password) {
					//if password matches, check if user has a session
					dbHelper.findOneByKey('Session',{user:user.id}, (err,session) => {
						if (err) {
							let now = new Date(),
								updateStatement = {
									ip: req.body.ip,
									key: guid(),
									start: now,
									end: now + now
								}
							//if there is a session, update it and send it back
							dbHelper.updateOne('Session',session.id, updateStatement, (err, session) => {
								if (err) {
									renderJSON(res,500,err)
								} else {
									renderJSON(res,200,'Success',session)
								}
							})
						} else {
							let now = new Date(),
								addStatement = {
									ip: req.body.ip,
									key: guid(),
									start: now,
									end: now + now,
									user: user.id,
									cart: []
								}
							//if not, make one and send it back
							dbHelper.addOne('Session',addStatement, (err,session) => {
								if (err) {
									renderJSON(res,500,err)
								} else {
									renderJSON(res,200,'Success',session)
								}
							})
						}
					})
				} else {
					renderJSON(res,500,'Password Mismatch')
				}
			}
		})
	})
Router.route('/logout') //kill session
	.post((req,res) => {
		let now = new Date(),
			updateStatement = {
				end: now
			}
		dbHelper.updateOne('Session',req.body.sessionid,updateStatement, (err,session) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'Success',session)
			}
		})
	})

Router.route('/create')
	.post((req,res) => {
		let today = new Date(),
			password = crypty.cipher(req.body.password) || null,
			addStatement = {
				password: password,
				email: req.body.email,
				firstname: req.body.firstname || null,
				lastname: req.body.lastname || null,
				nickname: req.body.nickname || null,
				company: req.body.company || null,
				website: req.body.company || null,
				sendNewsletter: req.body.sendNewsletter,
				registeredOn: today,
				registeredFrom: req.body.registeredFrom,
			},
			findStatement = {
				email: req.body.email,
			}
		dbHelper.findOneByKey('User',findStatement,(err,user) => {
			if (err) {
				//create the user
				dbHelper.addOne('User',addStatement,(err,user) => {
					if (err) {
						renderJSON(res,500,err)
					} else {
						renderJSON(res,200,'success',user)
					}
				})
			} else {
				//update the user with new info
				dbHelper.updateOne('User', user.id, updateStatement, (err, user) => {
					if (err) {
						renderJSON(res,500,err)
					} else {
						renderJSON(res,200,'success',user)
					}
				})
			}
		})
	})

Router.route('/:uid') //get + post + delete
	.get((req,res) => {
		dbHelper.findOne('User',req.params.uid, (err,user) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',user)
			}
		})
	})
	.post((req,res) => {
		let password = crypty.cipher(req.body.password),
			updateStatement = {
				password: password,
				email: req.body.email,
				firstname: req.body.firstname,
				lastname: req.body.lastname,
				nickname: req.body.nickname,
				company: req.body.company,
				website: req.body.company,
				sendNewsletter: req.body.sendNewsletter		
			}
		dbHelper.updateOne('User',req.params.uid,updateStatement, (err,user) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',user)
			}
		})
	})
	.delete((req,res) => {
		dbHelper.deleteOne('User',req.params.uid, (err,user) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success')
			}
		})
	})
module.exports = Router