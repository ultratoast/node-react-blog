const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	renderJSON = require('../helpers/renderJSON'),
	guid = require('../helpers/guid')

Router.route('/verify') //compare encrypted hardware ID from native app with key stored in db
	.post((req,res) => {
		let activationid = req.body.activationid,
			hash = req.body.hash
		dbHelper.findOne('Activation',activationid, (err, activation) =>{
			if (err) {
				renderJSON(res,500,err)
			} else {
				dbHelper.findOne('HashedID',activation.hash, (err,hashObj) => {
					if (hash === hashObj.hash) {
						renderJSON(res,200,'Success',{})
					} else {
						renderJSON(res,500,'Invalid')
					}
				})
			}
		})
	})
Router.route('/activate') //compare license key and save encrypted hardware ID from native app on first log in, create new activation entry in db
	.post((req,res) => {
		let hash = req.body.hash,
			key = req.body.key,
			email = req.body.email
		//first get the user
		dbHelper.findOneByKey('User',{email:email}, (err, user) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				let user = user
				//then verify that key is valid
				dbHelper.findOneByKey('Key',{license:key},(err,key) => {
					if (err) {
						renderJSON(res,500,err)
					} else {
						let license = key,
							today = new Date(),
							addStatement = {
								user: user.id,
								license: license.id,
								hash: hash,
								storedOn: today
							}
						//then save the hash in the system
						dbHelper.addOne('HashedID',addStatement, (err, hashObj) => {
							if (err) {
								renderJSON(res,500,err)
							} else {
								let addStatement = {
									user: user.id,
									license: license.id,
									hash: hashObj.id,
									isActive: true,
									activationDate: today
								}
								//finally create an activation and return it to the client for local storage
								dbHelper.addOne('Activation',addStatement, (err, activation) => {
									if (err) {
										renderJSON(res,500,err)
									} else {
										renderJSON(res,200,'Success',activation)
									}
								})
							}
						})
					}
				})
			}
		})
	})
Router.route('/deactivate') //disable license key, delete hashed ID
	.post((req,res) => {
		let activationid = req.body.activationid,
			hashid = req.body.hashid,
			keyid = req.body.keyid
		//remove activation 
		dbHelper.deleteOne('Activation',activationid, (err, activation) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				//find license key
				dbHelper.findOne('Key',keyid, (err, key) => {
					if (err) {
						renderJSON(res,500,err)
					} else {
						//find activation id in sub document of key
						let activations = key.activations,
							index = activations.indexOf(activationid)
							if (index > -1) {
								activations.splice(index, 1)
							} 
						let	updateStatement = {
								activations: activations
							}
						//save the key less the old activation
						dbHelper.updateOne('Key',updateStatement, (err, key) => {
							if (err) {
								renderJSON(res,500,err)
							} else {
								//finally, delete the hashed hardware id from our system
								dbHelper.deleteOne('HashedID', hashid, (err, hashObj) => {
									if (err) {
										renderJSON(res,500,err)
									} else {
										renderJSON(res,200,'succcess',{})
									}
								})
							}
						})
					}
				})
			}
		})
	})
Router.route('/generate') //generate a unique license key and save it in the database with 0 activations
	.post((req,res) => {
		let addStatement = {
			product: req.body.pid,
			license: guid(),
			soldOn: req.body.purchaseDate,
			activations: []
		}
		dbHelper.addOne('Key',addStatement,(err,key) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',key)
			}
		})
	})
module.exports = Router