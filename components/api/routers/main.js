const Express = require('express'),
	Router = Express.Router(),
	flog = require('./flog'),
	ecomm = require('./ecomm'),
	license = require('./license'),
	user = require('./user')


Router.use('/store',ecomm)

Router.use('/keys',license)

Router.use('/user',user)

Router.get('/', (req,res) => {
	res.send('Works!')
})

Router.all('*', flog) //i think its kind of fun to dump hackers into random blog/forum queries, don't you?

module.exports = Router