const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	product = require('./product'),
	paypal = require('./paypal'),
	renderJSON = require('../helpers/renderJSON')

Router.use('/paypal',paypal) //possible endpoint for paypal transactions

Router.use('/product',product)

Router.route('/category/:category?') //list all products, optionally from a specific category
	.get((req,res) => {
		let findStatement = {
			category: req.params.category || '*'
		}
		dbHelper.leanFind('Product',findStatement, (err,products) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',products)
			}
		})
	})

module.exports = Router