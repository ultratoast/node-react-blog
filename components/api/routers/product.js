const Express = require('express'),
	Router = Express.Router(),
	dbHelper = require('../helpers/db'),
	renderJSON = require('../helpers/renderJSON')

Router.route('/buy/:pid')
	.post((req,res) => {
		//get the product type just purchased
		dbHelper.findOne('Product',req.params.pid,(err,product) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				let productObj = product,
					userObj = Session.user,
					updateStatement = {
						purchases:  userObj.purchases.push(productObj) 
					}
				//add the product to the user's purchases
				dbHelper.updateOne('User',user.id,updateStatement, (err,user) => {
					if (err) {
						renderJSON(res,500,err)
					} else {
						let userUpdate = user,
							updateStatement = {
								quantity: productObj.quantity - 1
							}
						//Update the product in the database by removing one
						dbHelper.updateOne('Product',req.params.pid,updateStatement, (err, product) => {
							if (err) {
								renderJSON(res,500,err)
							} else {
								let today = new Date(),
									addStatement = {
										user: userUpdate.id,
										product: product.id,
										purchaseDate: today,
										purchasePrice: produce.price
									}
								productObj = product
								//register a new purchase
								dbHelper.addOne('Purchase',addStatement, (err,purchase) => {
									if (err) {
										renderJSON(res,500,err)
									} else {
										//update the user's cart to reflect the purchase
										dbHelper.updateOne('Cart', Session.cart.id, updateStatement, (err, cart) => {
											if (err) {
												renderJSON(res,500,err)
											} else {
												renderJSON(res,200,'success',{userUpdate,productObj,cart})
											}
										})
									}
								})
							}
						})
					}
				})
			}
		})
	})
	
Router.route('/add')
	.post((req,res) => {
		let addStatement = {
			name:req.body.name,
			categories: [],
			description: req.body.description,
			price: req.body.price,
			quantity: req.body.quantity,
			imageUrl: req.body.imageUrl,
			downloadUrl: req.body, downloadUrl,
			productKey: req.body.productKey,
			sku: req.body.sku
		}
		req.body.categories.map((category,i) => {
			addStatement.categories.push(category)
		})
		dbHelper.addOne('Product',addStatement, (err,product) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',product)
			}
		})
	})
Router.route('/:pid')
	.get((req,res) => {
		dbHelper.findOne('Product',req.params.pid, (err,product) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',product)
			}
		})
	})
Router.route('/:pid/edit')
	.post((req,res) => {
		let updateStatement = {
			name:req.body.name,
			categories: [],
			description: req.body.description,
			price: req.body.price,
			quantity: req.body.quantity,
			imageUrl: req.body.imageUrl,
			downloadUrl: req.body, downloadUrl,
			sku: req.body.sku
		}
		req.body.categories.map((category,i) => {
			addStatement.categories.push(category)
		})
		dbHelper.updateOne('Product',req.params.pid,updateStatement, (err,product) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',product)
			}
		})
	})
Router.route('/:pid/delete')
	.delete((req,res) => {
		dbHelper.deleteOne('Product',req.params.pid, (err, product) => {
			if (err) {
				renderJSON(res,500,err)
			} else {
				renderJSON(res,200,'success',product)
			}
		})
	})
	
module.exports = Router