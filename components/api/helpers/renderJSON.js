function renderJSON(res,status,message,data = {}) {
	res.status(status || 200).json({
		message: message,
		data: data
	})	
}

module.exports = renderJSON