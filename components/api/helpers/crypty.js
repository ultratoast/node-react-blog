"use strict";

const crypto = require('crypto');
const SECRET_PHRASE = "kendrickLamar";

const cipher = function(password) {
	var cipher = crypto.createCipher("aes192", SECRET_PHRASE)
	return cipher.update(password,"binary","hex") + cipher.final("hex")
}
const decipher = function(password) {
	var decipher = crypto.createDecipher("aes192", SECRET_PHRASE)
	return decipher.update(password, "hex", "binary") + decipher.final("binary")
}

module.exports = {cipher, decipher}