import { combineReducers  } from 'redux'

const blog = require('./blog/reducers.js').default

const mainReducer = combineReducers({
	blog
})

export default mainReducer