var React = require('react'),
	Link = require('react-router/lib/Link'),
	Post = require('./post.jsx')

var ShowBlog = React.createClass({
	getInitialState: function () {
	  return {blog: this.props.blog}
	},
	render: function() {
		var blog = this.state.blog
		return (
			<div>
				<Post blog={blog}/>
				<Link to={"/blogs/"+blog._id+"/edit"}>Edit</Link>
			</div>
		)
	}
})

module.exports = ShowBlog