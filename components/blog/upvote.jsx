var React = require('react')

var Upvote = React.createClass({
	getInitialState: function () {
	  return {blog: this.props.blog}
	},
	handleSubmit: function(e) {
	    e.preventDefault()
	    var blogId = React.findDOMNode(this.refs.blogId).value.trim()
	    this.props.onUpvoteSubmit({blogId:blogId})
	    // return
	},
	render: function() {
		var blog =this.state.blog
		return (
			<form onSubmit={this.handleSubmit} className="upvote-blog" method="POST" name="upvoteblog" action={"/api/blogs/"+blog._id+"/upvote"}>
				<input id="blogId" name="blogId" ref="blogId" value="{blog._id}" type="hidden"/>
				<button type="submit">Upvote</button>
			</form>
		)
	}
})

module.exports = Upvote