var React = require('react'),
	Upvote = require('./upvote.jsx'),
	Link = require('react-router/lib/Link')

var Post = React.createClass({
	getInitialState: function () {
	  return {post: this.props.post,postDate: this.props.postDate}
	},
	componentDidMount: function() {
		console.log('mounted!')
	},
	render: function(){
		var post = this.state.post,
			postDate = this.state.postDate ? <strong>this.state.postDate.toTimeString()</strong> : false
		return (
			<li className="post">	
				<Link to={"/blog/"+post._id}><h1>{post.name}</h1></Link>
				{postDate}
				<br />
				<span className="upvotes">Upvotes: {post.upvotes}</span>
				<p>{post.content}</p>	
				<Upvote post={post._id}/>
			</li>
		)
	}
})

module.exports = Post