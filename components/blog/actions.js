const fetch = require('isomorphic-fetch')

//action types

export const RECEIVE_BLOG_POSTS = 'RECEIVE_BLOG_POSTS'

export const REQUEST_BLOG_POSTS = 'REQUEST_BLOG_POSTS'

export const RECEIVE_ONE_BLOG_POST = 'RECEIVE_ONE_BLOG_POST'

export const REQUEST_ONE_BLOG_POST = 'REQUEST_ONE_BLOG_POST'

export const ADD_BLOG_POST = 'ADD_BLOG_POST'

export const EDIT_BLOG_POST = 'EDIT_BLOG_POST'

export const DELETE_BLOG_POST = 'DELETE_BLOG_POST'

export const UPVOTE_BLOG_POST = 'UPVOTE_BLOG_POST'

//other constants

export const BlogPostStatus = {
	DRAFT: 'DRAFT',
	PUBLISHED: 'PUBLISHED',
	DELETED: 'DELETED'
}

//action creators

export function addBlogPost(post) {
	return { type: ADD_BLOG_POST, post }
}

export function editBlogPost(post) {
	return { type: EDIT_BLOG_POST, post }
}

export function deleteBlogPost(post) {
	return { type: DELETE_BLOG_POST, post }
}

function requestPosts(blog) {
	return { type: REQUEST_BLOG_POSTS, blog }
}

function receivePosts(blog, json) {
	return {
		type: RECEIVE_BLOG_POSTS,
		blog,
		posts: json.data.childen.map(child => child.data),
		receivedAt: Date.now()
	}
}

function getPosts(blog) {
	return dispatch => {
		dispatch(requestPosts(blog))
		return fetch('/api/blog/latest')
			.then(response => response.json())
			.then(json => dispatch(recievedPosts(blog, json)))
	}
}

function shouldGetPosts(blog) {
	if (!blog || blog.length == 0) {
		return true
	} else if (blog.isFetching) {
		return false
	} else {
		return blog.didInvalidate
	}
}

export function getPostsIfNeeded(blog) {
	return (dispatch) => {
		if (shouldGetPosts(blog)) {
			return dispatch(getPosts(blog))
		}
	}
}