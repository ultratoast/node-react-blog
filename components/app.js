"use strict"

const Express = require('express'),
	app = Express(),
	React = require('react'),
	ReactDOMServer = require('react-dom/dist/react-dom-server'),
	fs = require('fs'),
	router = require('../routes/router'),
	routes = require('../routes/routes'),
	API = require('./api/routers/main.js')

app.engine('jsx',function(filePath,options,callback){
	let url = filePath.split(__dirname+'/')[1]
	url = url.split('.jsx')[0]
	console.log('url: '+url)
  	let file = require('./'+url),
  		File = React.createFactory(file),
  		component = File(options),
  		output = ReactDOMServer.renderToStaticMarkup(component)
  	return callback(null,output)
})

app.set('views','./components')

app.set('view engine','jsx')

app.use(Express.static('./dist'))

app.use('/api',API)

app.use(router)

app.listen(3000, function(){
	console.log('listening on port 3000')
})

module.exports = app