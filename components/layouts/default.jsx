var React = require('react'),
	SiteNav = require('./nav.jsx')

var Default = React.createClass({
	render: function() {
		return (
			<html>
				<head>
					<title>AudioFusion</title>
					<link rel="stylesheet" href="/css/styles.css"/>
				</head>
				<body>
					<section id="content">
						<SiteNav/>
						{this.props.children}
					</section>
					<script src="/js/client.bundle.js"></script>
				</body>
			</html>
		)
	}
})

module.exports = Default