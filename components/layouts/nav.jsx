var React = require('react'),
	Link = require('react-router/lib/Link')

var SiteNav = React.createClass({
	render: function() {
		return (
			<ul className="site-nav">
				<Link to="/">Home</Link>
				<Link to="/blog">Blog</Link>
				<Link to="/blog/archive">Archive</Link>
				<Link to="/blog/add">Add New</Link>
			</ul>
		)
	}
})

module.exports = SiteNav