const React = require('react'),
	ReactDOM = require('react-dom'),
	Router = require('react-router/lib/Router'),
	Provider = require('react-redux/lib/components/Provider'),
	browserHistory = require('react-router/lib/browserHistory'),
	match = require('react-router/lib/match'),
	routes = require('../routes/routes.js'),
	createStore = require('redux/lib/createStore').default,
	mainReducer = require('./main-reducer.js').default,
	content = document.getElementById('content')

match({ routes:routes, history:browserHistory }, (error, redirectLocation, renderProps) => {
	let initialState = document.getElementById('state'),
		stateJson = JSON.parse(initialState.dataset.state),
    	store = createStore(mainReducer, stateJson), 
    	provider = React.createFactory(Provider.default),
		router = React.createFactory(Router),
		component = provider({store: store},
			router(renderProps)
		)
	ReactDOM.render(component,content)
})