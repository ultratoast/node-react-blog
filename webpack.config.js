module.exports = {
  context: __dirname,
    entry: {
      // server: "./components/app.js",
      client: "./components/client.js"
    },
    output: {
        path: __dirname + "/dist/js",
        filename: "[name].bundle.js"
    },
 module: {
   loaders: [
     {
      test: /\.jsx?$/,
      exclude: /(node_modules|bower_components|api)/,
      loader: 'babel', // 'babel-loader' is also a legal name to reference
      query: {
        presets: ['react', 'es2015']
      }
    },
     {
      test: /\.js?$/,
      exclude: /(node_modules|bower_components|api)/,
      loader: 'babel', // 'babel-loader' is also a legal name to reference
      query: {
        presets: ['react', 'es2015']
      }
    },
     {
      test: /\.json$/,
      loaders: ['json']
     }
   ]
 },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json']
  }
};