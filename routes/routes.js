"use strict"
var isBrowser=new Function("try {return this===window;}catch(e){ return false;}")
if (!isBrowser()) {
	require('babel-register')({
	  presets: ['es2015','react']
	}) //fix es6 + jsx templates for require
}

const Default = require('../components/layouts/default.jsx'),
	HomeIndex = require('../components/home/index.jsx'),
	BlogIndex = require('../components/blog/index.jsx'),
	AddBlog = require('../components/blog/add.jsx'),
	ShowBlog = require('../components/blog/show.jsx'),
	EditBlog = require('../components/blog/edit.jsx'),
	ErrorPage = require('../components/misc/error.jsx'),
	BlogContainer = require('../components/blog/containers/getblog').default

const RouteConfig = [{
	path: '/',
	component: Default,
	indexRoute: {component: HomeIndex},
	// indexRoute: { onEnter: (nextState, replace) => replace('/home') },
	childRoutes: [
		{path: 'home', component:HomeIndex},
		{path: 'blog', component:BlogContainer},
		{
			path: 'blog',
			childRoutes: [
				{path: 'add', component: AddBlog},
				{path: ':id', component: ShowBlog},
				{path: ':id/edit', component: EditBlog}
			]
		},
		{
			path: 'misc',
			childRoutes: [
				{path: 'error', component:ErrorPage}
			]
		}
	]
}]

module.exports = RouteConfig